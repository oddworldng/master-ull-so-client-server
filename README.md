# Demonio con inotify y envío de estadísticas usando modelo cliente-servidor

## Descripción de la tarea

Tarea para subir el código del demonio incluyendo las opciones del inotify.

El objetivo es que el demonio escriba en un log todos los eventos que se produzcan en la monitorización de uno o varios directorios.

El **demonio** debería:

* Lanzarse y detenerse correctamente.
* Hacer el seguimiento de una o varias carpetas que se le pasen como parámetro.
* Mostrar como mínimo todos los eventos se produzcan correspondientes en la tabla colgada en el campusvirtual EventosInotify al primer bloque (de *IN_ACCESS* a *IN_OPEN*).
* **Opcional**: No volver a lanzar el demonio si ya está activo.
* **Opcional**: Monitorizar los eventos en todos los subdirectorios de los directorios especificados.
* **Opcional**: Si se añaden o borran subdirectorios la monitorización debería actualizarse dinámicamente. 

El **demonio** debe enviar un resumen de las estadísticas cada cierto tiempo a un **servidor**. El servidor muestra por pantalla o almacena en un fichero dicha información.

* **Opcional**: Comunicación bidireccional entre el servidor y el cliente.
* **Opcional**: Varios procesos o demonios cliente que envían información a un servidor.
* **Opcional**: Convertir en un demonio el código del servidor.

## Ejemplos de uso

* Compilar ficheros C

```
gcc midemoniod.c -o midemoniod
```

```
gcc server.c -o server
```

* Ejecutar el servidor

```
./server
```

* Ejecutar el demonio

```
./midemonio start <directorio_1> <directorio_2> ... <directorio_n>
```

* Detener el demonio

```
./midemonio stop
```

* Ver log del demonio

```
./midemonio status
```

* Ver estado del demonio

```
./midemonio check
```

## Autor

**Andrés Nacimiento García**
Máster en Ing. Informática en la Universidad de La Laguna (ULL).
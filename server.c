#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {

	/* Vars */
	int sockfd, newsockfd, i;
	socklen_t cli_longitud;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int wstatus = 5;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	bzero((char*) &serv_addr, sizeof(serv_addr));
	//portnumber = atoi(argv[1]);
	int portnumber = 8080;

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portnumber);
	 
	bind(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));
	listen(sockfd, 5); // 5 is the number of maximum connections
		 
	while(1){
		/* wait */
		wait(&wstatus);
		cli_longitud = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr*) &cli_addr, &cli_longitud);
		bzero(buffer, 256);
		i = read(newsockfd, buffer, 256);
		printf("%s\n", buffer);
	}

	/* Close connections */
	close(newsockfd);
	close(sockfd);

	return 0;
}


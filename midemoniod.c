#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

#include <sys/inotify.h>
#include <limits.h>
#define BUF_LEN (10*(sizeof(struct inotify_event)+NAME_MAX+1))

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

/* Client */
void client(char buffer[256]){

	/* Vars */
	int sockfd, i;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char *hostname = "localhost";
	int portnumber = 8080;
	
	/* Server conf */
	/* int socket(int domain, int type, int protocol) */
	sockfd = socket(AF_INET, SOCK_STREAM, 0); /* Open socket file descriptor */
	server = gethostbyname(hostname);
	bzero((char*) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	bcopy((char*)server->h_addr, (char*) &serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portnumber);

	/* Connection to server */
	/* int connect(int socket, const struct sockaddr *address, socklen_t address_len) */
	connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr));

	/* Send */
	i = write(sockfd, buffer, strlen(buffer));

	/* Close connection */
	close (sockfd);

}

/* Send event */
void sendEvent(struct inotify_event * e){

	char *mask;
	
	/* File was accessed (read()) */
	if (e->mask & IN_ACCESS){
		client("IN_ACCESS");
	}

	/* File metadata changed */
	if (e->mask & IN_ATTRIB){
		client("IN_ATTRIB");
	}

	/* File opened for writing was closed */
	if (e->mask & IN_CLOSE_WRITE){
		client("IN_CLOSE_WRITE");
	}

	/* File opened read-only was closed */
	if (e->mask & IN_CLOSE_NOWRITE){
		client("IN_CLOSE_NOWRITE");
	}

	/* File / directory created inside watched directory */
	if (e->mask & IN_CREATE){
		client("IN_CREATE");
	}

	/* File / directory deleted from within watched directory */
	if (e->mask & IN_DELETE){
		client("IN_DELETE");
	}

	/* Watched file / directory was itself deleted */
	if (e->mask & IN_DELETE_SELF){
		client("IN_DELETE_SELF");
	}

	/* File was modified */
	if (e->mask & IN_MODIFY){
		client("IN_MODIFY");
	}

	/* Watched file / directory was itself moved */
	if (e->mask & IN_MOVE_SELF){
		client("IN_MOVE_SELF");
	}

	/* File moved out of watched directory */
	if (e->mask & IN_MOVED_FROM){
		client("IN_MOVED_FROM");
	}

	/* File moved into watched directory */
	if (e->mask & IN_MOVED_TO){
		client("IN_MOVED_TO");
	}

	/* File was opened */
	if (e->mask & IN_OPEN){
		client("IN_OPEN");
	}

	/* -------------------- */
	
	/* Shorthand for all of the above input events */
	if (e->mask & IN_ALL_EVENTS){
		//client("IN_ALL_EVENTS", sockfd);
	}

	/* Shorthand for IN_MOVED_FROM | IN_MOVED_TO */
	if (e->mask & IN_MOVE){
		//client("IN_MOVE", sockfd);
	}

	/* Shorthand for IN_CLOSE_WRITE | IN_CLOSE_NOWRITE */
	if (e->mask & IN_CLOSE){
		//client("IN_CLOSE", sockfd);
	}

	/* -------------------- */

	/* Don't dereference symbolic link (since Linux 2.6.15) */
	if (e->mask & IN_DONT_FOLLOW){
		client("IN_DONT_FOLLOW");
	}

	/* Add events to current watch mask for pathname */
	if (e->mask & IN_MASK_ADD){
		client("IN_MASK_ADD");
	}

	/* Monitor pathname for just one event */
	if (e->mask & IN_ONESHOT){
		client("IN_ONESHOT");
	}

	/* Fail if pathname is not a directory (since Linux 2.6.15) */
	if (e->mask & IN_ONLYDIR){
		client("IN_ONLYDIR");
	}

	/* -------------------- */

	/* Watch was removed by application or by kernel */
	if (e->mask & IN_IGNORED){
		client("IN_IGNORED");
	}

	/* Filename returned in name is a directory */
	if (e->mask & IN_ISDIR){
		client("IN_ISDIR");
	}

	/* Overflow on event queue */
	if (e->mask & IN_Q_OVERFLOW){
		client("IN_Q_OVERFLOW");
	}

	/* File system containing object was returned */
	if (e->mask & IN_UNMOUNT){
		client("IN_UNMOUNT");
	}

	if (e->len > 0){
		//fprintf(f, "nombre=%s\n", e->name);
	}

}

/* Main */
int main (int argc, char * argv[]){

	int fd, wd, i;
	char buf[BUF_LEN];
	ssize_t num;
	char * e;
	struct inotify_event * event;
	fd = inotify_init();

	if (argc > 2){
		printf ("El demonio observará los siguientes directorios: \n");
		for (i = 2; i < argc; i++){
			printf ("* %s\n", argv[i]);
		}
	}else{
		printf ("No se ha pasado ningún directorio como parámetro.\n");
	}

	/* Our process ID and Session ID */
	pid_t pid, sid;

	/* Fork off the parent process */
	pid = fork();
	if (pid < 0) {
		exit(EXIT_FAILURE);
	}
	/* If we got a good PID, then we can exit the parent process. */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	/* Change the file mode mask */
	umask(0);
                
	/* Open logs */
	openlog(argv[0], LOG_NOWAIT | LOG_PID, LOG_USER); /* System log */
	syslog(LOG_NOTICE, "Demonio iniciado con éxito\n"); /* Syslog: LOG_NOTICE, LOG_INFO, LOG_ERROR */
	closelog();
	/* Close logs */
                
	/* Create a new SID for the child process */
	sid = setsid();
	if (sid < 0) {
		/* Log the failure */
		exit(EXIT_FAILURE);
	}

	/* Close out the standard file descriptors */
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	close(STDERR_FILENO);

	/* Daemon-specific initialization goes here */

	for (i = 2; i < argc; i++){
		wd = inotify_add_watch(fd, argv[i], IN_ALL_EVENTS);
	}
        
	/* The Big Loop */
	while (1) {
		/* Do some task here ... */
		num = read(fd, buf, BUF_LEN);
		for (e = buf; e < buf + num; ){
			event = (struct inotify_event *) e;
			sendEvent(event);
			e += sizeof(struct inotify_event) + event->len;
		}
	}
	exit(EXIT_SUCCESS);
}
